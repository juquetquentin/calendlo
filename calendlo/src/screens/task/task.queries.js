import { gql } from '@apollo/client';

export const CREATE_TASK_MUTATION = gql`
  mutation createTask(
    $title: String!
    $dateId: String
    $momentDate: String
    $beginHour: String!
    $duration: String!
    $userId: String!
    ) {
    createTask(title: $title, dateId: $dateId, momentDate: $momentDate, beginHour: $beginHour, duration: $duration, userId: $userId) {
      id
    }
  }
`;
