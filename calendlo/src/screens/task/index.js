import React, { useState } from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import moment from 'moment';

import { CREATE_TASK_MUTATION } from './task.queries';

import { useMutation } from '@apollo/client';

const task = ({ route, navigation }) => {
  const { user, dateId, momentDate } = route.params;
  const [title, setTitle] = useState('');
  const [beginHour, setBeginHour] = useState('');
  const [duration, setDuration] = useState('');

  console.log(momentDate);

  const [createTask, { loading }] = useMutation(CREATE_TASK_MUTATION, {
    onCompleted({ createTask }) {
      console.log('done', createTask);
      navigation.navigate('Calendar', { user });
    },
    onError(e) {
      console.log('Error :', e);
    },
  });

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => navigation.navigate('Calendar', { user })} style={styles.appButtonContainer}>
          <Text>Retour</Text>
      </TouchableOpacity>
      <View style={styles.inputView}>
        <TextInput
          style={styles.inputText}
          placeholder="Titre..."
          placeholderTextColor="#003f5c"
          onChangeText={(text) => setTitle(text)}
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          style={styles.inputText}
          placeholder="Durée (en nombre d'heures)"
          placeholderTextColor="#003f5c"
          onChangeText={(text) => setDuration(text)}
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          style={styles.inputText}
          placeholder="Heure de début (9, 10)"
          placeholderTextColor="#003f5c"
          onChangeText={(text) => setBeginHour(moment(momentDate).add(text, 'hours'))}
        />
      </View>
      <TouchableOpacity
        style={styles.loginBtn}
        onPress={() => createTask({ variables: { title, dateId, beginHour, duration, momentDate: momentDate.toString(), userId: user.id } })}>
        <Text style={styles.loginText}>Créer</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
    alignItems: 'center',
    justifyContent: 'center',
  },
  appButtonContainer: {
    alignSelf: 'flex-start',
    height: 30,

    position: 'absolute',
    top: 50,
    backgroundColor: "#fb5b5a",
    borderRadius: 15,
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 200,
    paddingVertical: 5,
    paddingHorizontal: 5
  },
  logo: {
    fontWeight: 'bold',
    fontSize: 50,
    color: '#fb5b5a',
    marginBottom: 40,
  },
  inputView: {
    width: '80%',
    backgroundColor: '#465881',
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    padding: 20,
  },
  inputText: {
    height: 50,
    color: 'white',
  },
  forgot: {
    color: 'white',
    fontSize: 11,
  },
  loginBtn: {
    width: '60%',
    backgroundColor: '#fb5b5a',
    borderRadius: 25,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
    marginBottom: 10,
  },
  loginText: {
    color: 'white',
  },
});

export default task;
