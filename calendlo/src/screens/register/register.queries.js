import { gql } from '@apollo/client';

export const SIGN_UP_MUTATION = gql`
  mutation signup($email: String, $password: String, $name: String) {
    signup(email: $email, password: $password, name: $name) {
      id
      email
      name
    }
  }
`;
