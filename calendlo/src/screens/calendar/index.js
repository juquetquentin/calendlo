import React, { useState, useEffect } from 'react';
import moment from 'moment';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  FlatList
} from 'react-native';
import CalendarPicker from 'react-native-calendar-picker';

const calendar = ({ route, navigation }) => {
    
  const { user } = route.params;
  const [dates, setDates] = useState(user.dates || []);
  const [tasks, setTasks] = useState([]);
  const [ready, setReady] = useState(false);
  const [currentUser, setCurrentUser] = useState(user || {})
  const [startDate, setStartDate] = useState('');
  const [dateId, setDateId] = useState('');

  const changeDate = (date) => {
    
    setStartDate(date.toString());

    const newTasks = dates.find(d => moment(d.date).isSame(date, 'days'));

    if (newTasks && newTasks.tasks) {
      console.log("NEW TASKS", newTasks);
      
      setTasks(newTasks.tasks);
      setDateId(newTasks.id);
    } else {
      setDateId('');
      setTasks([]);
    }
  }

  const renderItem = ({ item }) => {
    return (
      <View>
        <Text style={{color: '#FFFFFF', marginLeft: 5, marginTop: 5}}>{item.title} : from {moment(item.beginHour).format("hh:mm")} to {moment(item.beginHour).add(item.duration, "hours").format("hh:mm")}</Text>
      </View>
    )
  }

  useEffect(() => {
    console.log("Utilisation de l'effet");
    if (user && user.dates && !ready) {
      console.log("Oui on peut setup les tasks");
      const today = moment();
      console.log("AHAHHAHAHAHAH", user.dates);
      setDates(user.dates);
      changeDate(today);
      setReady(true);
    }
  }, [changeDate, ready, user]);
   
  return (
    <View style={styles.container}>
      <CalendarPicker
        onDateChange={(date) => changeDate(date)}
      />
      <SafeAreaView style={styles.taskContainer}>
        <TouchableOpacity onPress={() => navigation.navigate('Task', {user, dateId, momentDate: moment(startDate).toISOString()})} style={styles.appButtonContainer}>
          <Text style={styles.appButtonText}>+</Text>
        </TouchableOpacity>
        <FlatList
          data={tasks}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    marginTop: 50,
  },
  taskContainer: {
    flex: 1,
    backgroundColor: '#103f5c',
  },
  appButtonContainer: {
    alignSelf: 'flex-end',
    width: 30,
    height: 30,
    backgroundColor: "#fb5b5a",
    borderRadius: 15,
    marginRight: 10,
    marginTop: 10,
    paddingVertical: 5,
    paddingHorizontal: 5
  },
  appButtonText: {
    fontSize: 15,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  }
});

export default calendar;
