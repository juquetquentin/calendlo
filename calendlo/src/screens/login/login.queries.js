import { gql } from '@apollo/client';

export const LOGIN_MUTATION = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      id
      email
      name
      dates {
        id
        date
        workTime
        tasks {
          id
          title
          beginHour
          dateId
          duration
          completed
        }
      }
    }
  }
`;
