import createTask from "./scripts/createTask";
import updateTask from "./scripts/updateTask";

export default {
  Query: {
    test: async (parent, args, { prisma }) => {
      await prisma.task.deleteMany();
      console.log("tasks deleted");
      await prisma.date.deleteMany();
      console.log("dates deleted");
      await prisma.user.deleteMany();
      console.log("users deleted");
      return "ok";
    },
    task: async (parent, { id }, { prisma }) => await prisma.task({ id }),
    allTasks: async (parent, args, { prisma }) => await prisma.task.findMany(),
  },
  Mutation: {
    createTask,
    updateTask,
  },
  Task: {
    date: ({ id }, args, { prisma }) => {
      return prisma.task.findOne({ where: { id } }).date();
    },
  },
};
