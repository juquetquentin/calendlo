const updateTask = async (parent, args, { prisma }) => {
  // eslint-disable-next-line object-curly-newline
  const { userId, taskId, title, beginHour, duration, momentDate } = args;

  const task = await prisma.task.findOne({
    where: { id: taskId },
    include: { date: true },
  });

  if (task === null) return null;

  console.log("marche ? :", task.date);

  const date = await prisma.date.findOne({ where: { id: task.date.id } });

  if (momentDate === task.date.date) {
    const newDuration = parseInt(duration, 10);
    const taskDuration = parseInt(task.duration, 10);

    await prisma.date.update({
      where: { id: date.id },
      data: { workTime: date.workTime + (newDuration - taskDuration) },
    });

    return await prisma.task.update({
      where: { id: task.id },
      data: { title, beginHour, duration },
    });
  }
  const dates = await prisma.date.findMany({
    where: { date: momentDate },
  });

  if (dates.length) {
    const existingDate = dates[0];

    await prisma.date.update({
      where: { id: date.id },
      data: {
        tasks: {
          disconnect: [taskId],
        },
        workTime: date.workTime - parseInt(task.duration, 10),
      },
    });

    await prisma.date.update({
      where: { id: existingDate.id },
      data: {
        tasks: { connect: { id: task.id } },
        workTime: existingDate.workTime + parseInt(duration, 10),
      },
    });

    return await prisma.task.update({
      where: { id: taskId },
      data: { title, beginHour, duration },
    });
  }

  const dateData = {
    date: momentDate,
    workTime: parseInt(duration, 10),
    tasks: [],
  };

  const newDate = await prisma.date.create({
    data: { ...dateData, user: { connect: { id: userId } } },
  });

  const taskData = {
    title,
    beginHour,
    duration,
  };

  const newTask = await prisma.task.create({
    data: { ...taskData, date: { connect: { id: newDate.id } } },
  });

  return newTask;
};

export default updateTask;
