const mergeTasks = (tasks, newTask) => {
  const merged = tasks;

  merged.filter((el) => el != null);

  merged.push(newTask);

  return merged;
};

const createTask = async (parent, args, { prisma }) => {
  // eslint-disable-next-line object-curly-newline
  const { title, dateId, momentDate, beginHour, duration, userId } = args;

  if (dateId != null) {
    const date = await prisma.date.findOne({
      where: {
        id: dateId,
      },
    });

    if (date != null && userId === date.userId) {
      const taskData = {
        title,
        dateId: date.id,
        beginHour,
        duration,
      };

      const task = await prisma.task.create({
        data: { ...taskData, date: { connect: { id: dateId } } },
      });

      const tasks = mergeTasks(date.tasks, task);

      await prisma.date.update({
        tasks,
        workTime: date.workTime + parseInt(duration, 10),
      });

      return task;
    }
  }

  const dateData = {
    date: momentDate,
    workTime: parseInt(duration, 10),
    tasks: [],
  };

  const date = await prisma.date.create({
    data: { ...dateData, user: { connect: { id: userId } } },
  });

  const taskData = {
    title,
    beginHour,
    duration,
  };

  const task = await prisma.task.create({
    data: { ...taskData, date: { connect: { id: date.id } } },
  });

  return task;
};

export default createTask;
