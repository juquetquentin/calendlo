import signup from "./scripts/signUp";
import login from "./scripts/login";
import updateUser from "./scripts/updateUser";

export default {
  Query: {
    user: async (parent, { id }, { prisma }) => await prisma.user({ id }),
    allUsers: async (parent, args, { prisma }) => await prisma.user.findMany(),
  },
  Mutation: {
    login,
    signup,
    updateUser,
  },
  User: {
    dates: ({ id }, args, { prisma }) => {
      return prisma.user.findOne({ where: { id } }).dates();
    },
  },
};
