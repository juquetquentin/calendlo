const login = async (parent, args, { prisma }) => {
  const { email, password } = args;

  const existing = await prisma.user.findMany({
    where: {
      email,
      password,
    },
  });

  if (existing.length) {
    return existing[0];
  }

  return null;
};

export default login;
