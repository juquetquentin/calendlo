const updateUser = async (parent, args, { prisma }) => {
  const { name, email, workTime } = args;

  const data = {
    name,
    email,
    workTime,
  };

  return await prisma.updateUser({ data, where: { id: args.id } });
};

export default updateUser;
