const signUp = async (parent, args, { prisma }) => {
  const { name, email, password } = args;

  console.log("Signup !", args);

  const existing = await prisma.user.findMany({
    where: {
      email,
    },
  });

  if (existing.length) {
    console.log(
      // eslint-disable-next-line comma-dangle
      `Error user already existing with mail : ${email}. Can't create account`
    );
    return null;
  }

  const user = await prisma.user.create({
    data: {
      name,
      email,
      password,
      dates: [],
    },
  });

  console.log(user);

  return user;
};

export default signUp;
