import { mergeResolvers } from "merge-graphql-schemas";

import userResolver from "./user/resolvers";
import dateResolver from "./date/resolvers";
import taskResolver from "./task/resolvers";

const resolvers = [dateResolver, userResolver, taskResolver];

export default mergeResolvers(resolvers);
