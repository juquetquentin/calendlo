import moment from "moment";

export default {
  Query: {
    test: async () => {
      const now = moment()
        .utc()
        .date("14")
        .month("3") // -> April
        .year("2012")
        .startOf("day");

      console.log("now:", now);

      // const str = now.toISOString();

      // console.log(str);
      // console.log("str:", str);

      // const neo = moment(str);

      // console.log("neo:", neo);

      // console.log("work:", neo.day());

      return "toto";
    },
    allDates: async (parent, args, { prisma }) => await prisma.date.findMany(),
  },
  Mutation: {
    updateDate: async (parent, args, prisma) => {
      if (args.admin) {
        const date = await prisma.findOne({ id: args.id });

        console.log(date);

        return await prisma.updateDate({
          where: { id: args.id },
          data: { ...args },
        });
      }
      return null;
    },
  },
  Date: {
    tasks: ({ id }, args, { prisma }) => {
      return prisma.date.findOne({ where: { id } }).tasks();
    },
    user: ({ id }, args, { prisma }) => {
      return prisma.date.findOne({ where: { id } }).user();
    },
  },
};
