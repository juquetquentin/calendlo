# Migration `20200524172319-init`

CREATE TABLE "public"."User" (
"id" text  NOT NULL ,"name" text  NOT NULL ,
    PRIMARY KEY ("id"))

CREATE TABLE "public"."Todo" (
"completed" boolean  NOT NULL DEFAULT false,"id" text  NOT NULL ,"text" text  NOT NULL ,"userId" text   ,
    PRIMARY KEY ("id"))

ALTER TABLE "public"."Todo" ADD FOREIGN KEY ("userId")REFERENCES "public"."User"("id") ON DELETE SET NULL  ON UPDATE CASCADE


# Migration `20200527122936-user-task-date`

ALTER TABLE "public"."User" ADD COLUMN "email" text  NOT NULL DEFAULT 'test@test.fr',
ADD COLUMN "password" text  NOT NULL DEFAULT 'azerazer';

ALTER TABLE "public"."Todo" DROP CONSTRAINT IF EXiSTS "Todo_userId_fkey";

ALTER TABLE "public"."Task" ADD FOREIGN KEY ("userId")REFERENCES "public"."User"("id") ON DELETE SET NULL  ON UPDATE CASCADE

DROP TABLE "public"."Todo";


# Migration `20200605153355-user-email-unique`

CREATE UNIQUE INDEX "User.email" ON "public"."User"("email")


# Migration `20200605155920-forgot-work-time`

DROP INDEX "public"."User.email"

ALTER TABLE "public"."User" ADD COLUMN "workTime" integer  NOT NULL DEFAULT 8;


# Migration `20200613120908-change-month_nbr-to-month-nbr`

ALTER TABLE "public"."Date" DROP COLUMN "month_nbr",
ADD COLUMN "monthNbr" integer  NOT NULL ;


# Migration `20200625160750-datamodels-reword`


ALTER TABLE "public"."Date" DROP COLUMN "day",
DROP COLUMN "month",
DROP COLUMN "monthNbr",
DROP COLUMN "year",
ADD COLUMN "date" text  NOT NULL ,
ADD COLUMN "userId" text   ,
ADD COLUMN "workTime" integer  NOT NULL ;

ALTER TABLE "public"."Task" DROP CONSTRAINT IF EXiSTS "Task_userId_fkey",
DROP COLUMN "date",
DROP COLUMN "endTime",
DROP COLUMN "startTime",
DROP COLUMN "userId",
ADD COLUMN "beginHour" text  NOT NULL ,
ADD COLUMN "dateId" text  NOT NULL ,
ADD COLUMN "duration" text  NOT NULL ;

ALTER TABLE "public"."Date" ADD FOREIGN KEY ("userId")REFERENCES "public"."User"("id") ON DELETE SET NULL  ON UPDATE CASCADE

ALTER TABLE "public"."Task" ADD FOREIGN KEY ("dateId")REFERENCES "public"."Date"("id") ON DELETE SET NULL  ON UPDATE CASCADE