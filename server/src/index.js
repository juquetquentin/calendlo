import "dotenv/config";
import { PrismaClient } from "@prisma/client";
import { GraphQLServer } from "graphql-yoga";
import { importSchema } from "graphql-import";
import path from "path";
import moment from "moment";

import resolvers from "./graphql/resolvers";
import { PORT } from "./config";

moment().format();

const prisma = new PrismaClient();

const typeDefs = importSchema(
  // eslint-disable-next-line comma-dangle
  path.resolve(__dirname, "graphql", "schema.graphql")
);

const server = new GraphQLServer({
  typeDefs,
  resolvers,
  context: { prisma },
  // middlewares: [permissions],
});

server.start(() => console.log(`Server running on localhost:${PORT} !`));
