import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

const main = async () => {
  const sasha = await prisma.user.create({
    data: {
      name: "Sasha",
    },
  });

  console.log(sasha);

  const johnny = await prisma.users.create({
    data: {
      name: "Johnny",
      todos: {
        create: [
          {
            text: "Do dishes",
          },
          {
            text: "Walk the dog",
          },
        ],
      },
    },
  });

  console.log(johnny);
};

main()
  .catch((e) => console.error(e))
  .finally(async () => {
    await prisma.disconnect();
  });
